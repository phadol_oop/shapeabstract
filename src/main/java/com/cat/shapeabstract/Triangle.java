/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cat.shapeabstract;

/**
 *
 * @author Black Dragon
 */
public class Triangle extends Shape {

    private int h;
    private int b;

    public Triangle(int h, int b) {
        super("Triangle");
        this.h = h;
        this.b = b;
    }

    @Override
    public double calArea() {
        return h * b * 0.5;
    }

    public int getH() {
        return h;
    }

    public int getB() {
        return b;
    }

    public void setH(int h) {
        this.h = h;
    }

    public void setB(int b) {
        this.b = b;
    }

    @Override
    public String toString() {
        return "Triangle{" + "hight=" + h + ", base=" + b + '}';
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cat.shapeabstract;

/**
 *
 * @author Black Dragon
 */
public class Rectangle extends Shape {

    private int h;
    private int w;

    public Rectangle(int h, int w) {
        super("Rectangle");
        this.h = h;
        this.w = w;
    }

    public Rectangle(String n, int h, int w) {
        super("Square");
        this.h = h;
        this.w = w;
    }

    @Override
    public double calArea() {
        return h * w;
    }

    public int getA() {
        return h;
    }

    public int getB() {
        return w;
    }

    public void setA(int a) {
        this.h = a;
    }

    public void setB(int b) {
        this.w = b;
    }

    @Override
    public String toString() {
        return "Rectangle{" + "hight=" + h + ", width=" + w + '}';
    }

}

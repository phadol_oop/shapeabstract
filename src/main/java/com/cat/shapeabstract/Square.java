/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cat.shapeabstract;

/**
 *
 * @author Black Dragon
 */
public class Square extends Rectangle {

    private int side;

    public Square(int side) {
        super("Square", side, side);
        this.side = side;

    }

    @Override
    public String toString() {
        return "Square{side=" + side + '}';
    }

}
